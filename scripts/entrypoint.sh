#!/usr/bin/env bash

wait_for_db() {
  while ! pg_isready -d "$DATABASE_URL"; do
    echo "Postgres is unavailable - sleeping for 5 seconds"
    sleep 5
  done
}

wait_for_db

uvicorn app.main:app --reload --workers 1 --host 0.0.0.0 --port 8000