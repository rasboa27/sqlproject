from typing import List

from sqlmodel import select

from app.db.create_db import (
    create_db_and_table,
    create_db_entities,
    get_all_entities,
    session,
    get_specific_product_type_entity,
    get_specific_product_entity,
)
from app.functions import create_entities, select_all_product_types

from fastapi import FastAPI

from app.model import ProductType, Product, ProductCreate, ProductTypeCreate

app = FastAPI()
create_db_and_table()


@app.post("/create_productType", response_model=ProductType)
async def create_product_type(productType: ProductTypeCreate):
    new_prod_type = ProductType.from_orm(productType)
    create_db_entities(new_prod_type)


@app.post("/create_product", response_model=ProductCreate)
async def create_product(product: ProductCreate):
    statement = select(ProductType).where(ProductType.id == product.product_type)
    prod_type = session.exec(statement).one()
    product.product_type = prod_type.name

    print(product.product_type)
    new_product = Product.from_orm(product)
    create_db_entities(new_product)
    pass


@app.get("/all_productType", response_model=List[ProductType])
async def get_all_product_type():
    product_types = get_all_entities(ProductType)
    return product_types


@app.get("/all_product", response_model=List[Product])
async def get_all_product():
    products = get_all_entities(Product)
    return products


@app.put("/productType/{id}")
async def update_product_type(id: int, productType: ProductTypeCreate):
    prod_type = get_specific_product_type_entity(id)
    prod_type.name = productType.name
    # update linked product
    related_prod = get_specific_product_entity(prod_type.name)
    related_prod.product_type = productType.name

    session.commit()


@app.delete("/productType/{id}")
async def delete_product_type(id: int):
    prod_type = get_specific_product_type_entity(id)
    session.delete(prod_type)


def main():
    create_db_and_table()
    create_entities()
    select_all_product_types()


# select_specific_product_type()
# select_first_two_product_type()
# select_next_two_product_type()
# update_specific_product_type()
# delete_specific_product_type ()


if __name__ == "__main__":
    main()
