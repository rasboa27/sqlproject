from sqlmodel import Session, select

from sqlmodel.sql.expression import Select, SelectOfScalar

from app.db.create_db import create_db_entities, engine
from app.model import ProductType, Product

SelectOfScalar.inherit_cache = True  # type: ignore
Select.inherit_cache = True  # type: ignore


def create_entities():
    """create objects and fill in to database"""

    product_type_one = ProductType(name="Burger")
    product_type_two = ProductType(name="Drink")

    product_one = Product(name="Hamburger", product_type=product_type_one.id)

    product_two = Product(name="Sprite", product_type=product_type_two.id)

    create_db_entities([product_type_one, product_type_two, product_one, product_two])


def select_all_product_types():
    """get all the product type available in the database"""
    with Session(engine) as session:
        statement = select(Product, ProductType).where(
            Product.product_type == ProductType.id
        )
        results = session.exec(statement)
        all_product_types = results.all()
        for prod in all_product_types:
            print("Product: ", prod)


def select_specific_product_type():
    """how to select a specific element from the db"""
    with Session(engine) as session:
        statement = select(ProductType).where(ProductType.name == "Hamburger")
        results = session.exec(statement)
        # can use results.first() to get the first element in db
        specific_product_type = results.first()
        print("specific product type : ", specific_product_type)


def select_first_two_product_type():
    """retrieve only the first 2 entry of db table product type"""
    with Session(engine) as session:
        statement = select(ProductType).limit(2)
        results = session.exec(statement)
        first_two_product_type = results.all()
        print("first_two_product_type : ")
        for prod in first_two_product_type:
            print(prod)


def select_next_two_product_type():
    """retrieve only the next 2 entry of db table product type"""
    with Session(engine) as session:
        statement = select(ProductType).offset(2).limit(2)
        results = session.exec(statement)
        next_two_product_type = results.all()
        print("next_two_product_type : ")
        for prod in next_two_product_type:
            print(prod)


def update_specific_product_type():
    """update name of the first product-type burger in the db table"""
    with Session(engine) as session:
        # get product to update
        statement = select(ProductType).where(ProductType.name == "Burger")
        results = session.exec(statement)
        product = results.first()
        print("Before updating : ", product)
        product.name = "New Burger"
        session.add(product)
        session.commit()
        # to enable to view what the new updated element
        session.refresh(product)
        print("After updating : ", product)


def delete_specific_product_type():
    """delete the first apple name present in db product type"""
    with Session(engine) as session:
        statement = select(ProductType).where(ProductType.name == "apple")
        results = session.exec(statement)
        product = results.first()
        session.delete(product)
        session.commit()
        print("Product deleted : ", product)
