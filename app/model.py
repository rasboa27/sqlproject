from typing import Optional


from sqlmodel import Field, SQLModel


class ProductType(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str


class ProductTypeCreate(SQLModel):
    name: str


class Product(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str  # =Field(sa_column=Column('name',String,unique=True))
    # many product can have one product type
    product_type: Optional[str] = Field(default=None, foreign_key="producttype.id")


class ProductCreate(SQLModel):
    name: str
    product_type: Optional[int] = None
