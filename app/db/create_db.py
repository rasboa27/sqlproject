from sqlmodel import SQLModel, create_engine, Session, select
import os

from app.model import ProductType, Product

sql_file_name = "database.db"
conn_string = f"sqlite:///{sql_file_name}"
# = "postgresql://{user}:{password}@{host}:{port}/{db}"

DATABASE_URL = os.environ.get("DATABASE_URL")
engine = create_engine(DATABASE_URL, echo=False)
session = Session(bind=engine)


def create_db_and_table():
    SQLModel.metadata.create_all(engine)


def create_db_entities(my_object: object):
    """
    :param my_object: list of sql objects
    """
    print(
        'New {object} object "{details}" inserted into db table'.format(
            object=my_object.__class__.__name__, details=getattr(my_object, "name")
        )
    )
    with Session(engine) as session:
        session.add(my_object)
        session.commit()
        session.refresh(my_object)


def get_all_entities(my_object: object):
    statement = select(my_object)
    results = session.exec(statement).all()
    return results


def get_specific_product_type_entity(id: int):
    statement = select(ProductType).where(ProductType.id == id)
    prod_type = session.exec(statement).one()
    return prod_type


def get_specific_product_entity(name: str):
    statement = select(Product).where(Product.product_type == name)
    prod = session.exec(statement).one()
    return prod
